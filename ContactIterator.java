package com.mycompany.contactiterator;

import java.util.*;

public class ContactIterator{

    public static void main(String[] args) {
        System.out.println("CONTACTS");
        
        ArrayList<Contact> contact = new ArrayList<>();
        contact.add(new Contact(987654321, "Fernando"));
        contact.add(new Contact(987532146, "Rodrigo"));
        contact.add(new Contact(987456321, "Pepe"));
        contact.add(new Contact(987512364, "Juan"));

        System.out.println(printArray(contact));
    }
    
    private static String printArray(ArrayList<Contact> products) {
    String list = "";
    Iterator<Contact> iterator = products.iterator();
    while (iterator.hasNext()) {
        list += iterator.next().toString();
    }
    return list;
  }
}
